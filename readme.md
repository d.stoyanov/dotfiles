# Automated system setup and configuration

A repository to keep my configuration files and task specific setup process organized and portable.

The setup/configuration is split into more manageable chunks centered around a specific task. For example `vim` or `shell` or `docker`. Each of these tasks is responsible for setting up the needed packages as well as handling the needed configuration.

Here is a list of all available tasks:

- vim
- shell
- docker

# Install

**Install base packages**

Before we continue the whole process depends on `git` and `ansible` so we need to set them up before proceeding.

```sh
sudo pacman -S git ansible
ansible-galaxy collection install community.general
ansible-galaxy collection install kewlfft.aur
```

**Fetch the dotfiles from the repo**

```sh
git clone https://gitlab.com/d.stoyanov/dotfiles.git ~/.dotfiles
```

**Setup a machine from scratch**

This should be used to perform a full setup after a fresh OS install.

```sh
cd  ~/.dotfiles
./bin/setup

ansible-playbook setup.yml --ask-become-pass
```

**Execute a specific task**

If we want to execute just a specific task we can do it using the following command:

```sh
ansible-playbook setup.yml --ask-become-pass --tags "core"
```
