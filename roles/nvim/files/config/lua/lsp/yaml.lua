local lsp = require("lspconfig")

-- configure the language server
lsp.yamlls.setup({
    on_attach = function(client)
        client.server_capabilities.documentFormattingProvider = false
    end,
    yaml = {
        schemas = {
            ["https://json.schemastore.org/github-workflow.json"] = "/.github/workflows/*",
            ["https://raw.githubusercontent.com/instrumenta/kubernetes-json-schema/master/v1.18.0-standalone-strict/all.json"] = "/*.k8s.yml",
            ["https://raw.githubusercontent.com/ansible/schemas/main/f/ansible.json"] = "ansible/*.yml",
        },
    },
})
