local lsp = require("lspconfig")

-- configure the language server
lsp.svelte.setup({
    -- on_attach = function(client)
    --     -- disable tsserver formatting as we are doing it using null-ls
    --     client.server_capabilities.documentFormattingProvider = false
    --     client.server_capabilities.documentRangeFormattingProvider = false
    -- end,
    -- settings = {
    --     svelte = {
    --         plugin = {
    --             svelte = {
    --                 compilerWarnings = {
    --                     ["element_invalid_self_closing_tag"] = "ignore",
    --                 },
    --             },
    --         },
    --     },
    -- },
})
