-- do not load configurations unless the lspconfig is loaded
local has = require("utils").has
if not has("lspconfig") then
    return
end

-- load the defaults
require("lsp.defaults")
require("lsp.null-ls")

-- language specific configs
require("lsp.lua")
require("lsp.js")
require("lsp.html")
require("lsp.json")
require("lsp.yaml")
require("lsp.css")
require("lsp.sh")
require("lsp.perl")
require("lsp.python")
require("lsp.go")
require("lsp.svelte")
require("lsp.terraform")
require("lsp.rust")
