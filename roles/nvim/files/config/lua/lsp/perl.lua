local lsp = require("lspconfig")

lsp.perlpls.setup({
    cmd = { "/usr/bin/site_perl/pls" },
    settings = {
        perl = {
            perlcritic = { enabled = true },
        },
    },
})
