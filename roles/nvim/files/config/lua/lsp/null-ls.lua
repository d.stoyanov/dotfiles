local lsp = require("lspconfig")
local ls = require("null-ls")

local b = ls.builtins
local sources = {
    -- prettier
    b.formatting.prettierd.with({
        filetypes = {
            "javascript",
            "javascriptreact",
            "typescript",
            "typescriptreact",
            "svelte",
            "markdown",
            "html",
            "css",
            "scss",
            "less",
            "sass",
            "json",
            "jsonp",
            "jsonc",
            "yaml",
        },
    }),

    -- eslint
    -- b.diagnostics.eslint.with({}),

    -- lua
    b.formatting.stylua.with({
        args = {
            "--indent-type",
            "Spaces",
            "--column-width",
            "80",
            "-s",
            "-",
        },
    }),

    -- shell
    b.formatting.shfmt.with({ args = { "-i", "4" } }),
    b.diagnostics.shellcheck,

    -- yaml
    b.diagnostics.yamllint.with({
        args = {
            "-d",
            "{extends: default, rules: {line-length: disable, document-start: disable}}",
            "--format",
            "parsable",
            "-",
        },
    }),

    -- misc
    b.code_actions.gitsigns,
    b.hover.dictionary,
}

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
ls.setup({
    on_attach = function(_, bufnr)
        vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
                local all_clients = vim.lsp.buf_get_clients(bufnr)

                all_clients = vim.tbl_filter(function(client)
                    return client.supports_method("textDocument/formatting")
                end, all_clients)

                if #all_clients > 0 then
                    vim.lsp.buf.format({ timeout_ms = 2000 })
                end
            end,
        })
    end,
    sources = sources,
    root_dir = lsp.util.root_pattern("package.json", ".git"),
    debug = true,
})
