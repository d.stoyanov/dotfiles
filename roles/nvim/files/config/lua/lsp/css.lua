local lsp = require("lspconfig")

-- configure the language server
lsp.cssls.setup({
    init_options = { provideFormatter = false },
})
