local has = require("utils").has
local lsp = require("lspconfig")

-- vim.lsp.set_log_level("debug")

-- configure the diagnostic symbols
local signs = {
    Error = "",
    Warning = "",
    Warn = "",
    Hint = "",
    Information = "",
    Info = "",
}
for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- extend the default on_attach function
-- local on_attach = function(client)
--     local capabilities = client.server_capabilities
--
--     if capabilities.document_highlight and has("illuminate") then
--         require("illuminate").on_attach(client)
--     end
-- end

-- local capabilities = vim.lsp.protocol.make_client_capabilities()
-- capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)
--
-- capabilities.textDocument.completion.completionItem.snippetSupport = true
-- capabilities.textDocument.completion.completionItem.resolveSupport = {
--     properties = {
--         "documentation",
--         "detail",
--         "additionalTextEdits",
--     },
-- }

-- lsp.util.default_config = vim.tbl_extend(
--     "force",
--     lsp.util.default_config,
--     { on_attach = on_attach, capabilities = capabilities }
-- )
