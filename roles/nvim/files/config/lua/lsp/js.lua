local lsp = require("lspconfig")
-- local util = require("lspconfig/util")

local exclude_diagnostics = {
    80001, -- convert to es6 module
    6133, -- unused variables (handled by eslint)
}

-- local function root_pattern_excludes(opt)
--     local root = opt.root
--     local exclude = opt.exclude
--
--     local function matches(path, pattern)
--         return 0 < #vim.fn.glob(util.path.join(path, pattern))
--     end
--
--     return function(startpath)
--         return util.search_ancestors(startpath, function(path)
--             return matches(path, root) and not matches(path, exclude)
--         end)
--     end
-- end

-- configure the language server
lsp.ts_ls.setup({
    -- root_dir = lsp.util.root_pattern("package.json"),
    on_attach = function(client)
        -- disable tsserver formatting as we are doing it using null-ls
        client.server_capabilities.documentFormattingProvider = false
        client.server_capabilities.documentRangeFormattingProvider = false
    end,
    handlers = {
        ["textDocument/publishDiagnostics"] = function(_, result, ctx, config)
            -- ignore some tsserver diagnostic codes
            if result.diagnostics ~= nil then
                local idx = 1
                while idx <= #result.diagnostics do
                    local code = result.diagnostics[idx].code
                    local match = false

                    for _, exclude in pairs(exclude_diagnostics) do
                        if code == exclude then
                            table.remove(result.diagnostics, idx)
                            match = true
                        end
                    end

                    if not match then
                        idx = idx + 1
                    end
                end
            end

            return vim.lsp.diagnostic.on_publish_diagnostics(
                _,
                result,
                ctx,
                config
            )
        end,
    },
})

-- lsp.denols.setup({
--     root_dir = lsp.util.root_pattern("deno.json", "deno.jsonc"),
--     single_file_support = false,
--     init_options = {
--         enable = true,
--         lint = true,
--         unstable = true,
--     },
--     on_attach = function(client)
--         -- disable tsserver formatting as we are doing it using null-ls
--         client.server_capabilities.documentFormattingProvider = false
--         client.server_capabilities.documentRangeFormattingProvider = false
--     end,
-- })

-- setup the eslint server
lsp.eslint.setup({
    on_attach = function(client)
        client.server_capabilities.documentFormattingProvider = false
        client.server_capabilities.documentRangeFormattingProvider = false
    end,
})
