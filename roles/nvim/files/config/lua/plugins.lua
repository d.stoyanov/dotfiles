local cmd = vim.cmd

cmd("packadd packer.nvim")
require("packer").startup(function(use)
    -- core modules
    -- use("nathom/filetype.nvim")
    use({ "wbthomason/packer.nvim", opt = true })
    use("nvim-lua/plenary.nvim")
    use({ "nvim-lua/popup.nvim", after = "plenary.nvim" })

    -- interface
    use("sainnhe/gruvbox-material")
    use("kyazdani42/nvim-web-devicons")
    use({
        "nvim-treesitter/nvim-treesitter",
        run = function()
            local ts_update =
                require("nvim-treesitter.install").update({ with_sync = true })
            ts_update()
        end,
    })
    use("nvim-treesitter/playground")

    -- lsp
    use("neovim/nvim-lspconfig")
    use("jose-elias-alvarez/null-ls.nvim")
    use("lewis6991/gitsigns.nvim")
    -- use("jose-elias-alvarez/nvim-lsp-ts-utils")
    use("onsails/lspkind-nvim")
    use("nvimdev/lspsaga.nvim")
    use("weilbith/nvim-code-action-menu")

    -- Autocomplete
    use("hrsh7th/nvim-cmp")
    use("hrsh7th/cmp-nvim-lsp")
    use("hrsh7th/cmp-buffer")
    use("hrsh7th/cmp-path")
    use("hrsh7th/cmp-cmdline")
    use("ray-x/lsp_signature.nvim")

    -- snippets
    use("hrsh7th/cmp-vsnip")
    use("hrsh7th/vim-vsnip")
    use("hrsh7th/vim-vsnip-integ")
    use("rafamadriz/friendly-snippets")

    -- tmux
    use({
        "aserowy/tmux.nvim",
        config = function()
            require("tmux").setup({ copy_sync = { enable = false } })
        end,
    })
    use({ "preservim/vimux" })

    -- Git
    use("f-person/git-blame.nvim")
    use("tpope/vim-fugitive")

    -- file navigation
    use("kyazdani42/nvim-tree.lua")
    use("nvim-telescope/telescope.nvim")

    -- auto closing brackets and tags
    use("windwp/nvim-autopairs")
    use("windwp/nvim-ts-autotag")

    -- language specific
    use("AndrewRadev/tagalong.vim")
    use("othree/html5.vim")
    use("plasticboy/vim-markdown")
    use("Glench/Vim-Jinja2-Syntax")
    use("tjvr/vim-nearley")
    use("hashivim/vim-terraform")
    use("saltstack/salt-vim")

    -- help
    -- use("folke/which-key.nvim")

    -- editing
    use("mbbill/undotree")
    use("tpope/vim-surround")
    use("justinmk/vim-sneak")
    use("lukas-reineke/indent-blankline.nvim")
    use("andymass/vim-matchup")
    use("reedes/vim-pencil")

    -- term
    use("numtostr/FTerm.nvim")

    -- handling comments
    use("JoosepAlviste/nvim-ts-context-commentstring")
    -- use("b3nj5m1n/kommentary")
    use("numToStr/Comment.nvim")

    -- misc
    use("martini97/project-config.nvim")
    use("glepnir/galaxyline.nvim")
    use("rmagatti/auto-session")
    use("moll/vim-bbye")
    use("rrethy/vim-illuminate")
    use("romainl/vim-cool")
end)

-- plugin configuration
require("lsp")
require("plugins.lspsaga")
require("plugins.code-action-menu")
require("plugins.lspkind")
require("plugins.treesitter")
require("plugins.telescope")
require("plugins.tree")
require("plugins.cmp")
require("plugins.lsp_signature")
-- -- require("plugins.kommentary")
require("plugins.comment")
require("plugins.galaxyline")
require("plugins.session")
require("plugins.autopairs")
require("plugins.autotag")
require("plugins.illuminate")
require("plugins.git-blame")
-- require("plugins.which-key")
require("plugins.markdown")
require("plugins.wrap")
require("plugins.sneak")
require("plugins.term")
require("plugins.indent-line")
require("plugins.pencil")
require("plugins.tmux")
