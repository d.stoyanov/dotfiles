local export = {}

--
-- Load some env variables
--
export.env = {}
export.env.user = vim.fn.expand("$USER")
export.env.home = vim.fn.expand("$HOME")
export.env.data = vim.fn.stdpath("data")
export.env.config = vim.fn.stdpath("config")

--
-- Test if a module exists
--
export.has = function(...)
    local status, _ = pcall(require, ...)
    if status then
        return true
    else
        return false
    end
end

--
-- Convenience function to create auto groups
--
export.autogroup = function(group, commands)
    vim.cmd("augroup " .. group)
    vim.cmd("autocmd!")

    for _, def in pairs(commands) do
        local command = table.concat(vim.tbl_flatten({ "autocmd", def }), " ")
        vim.cmd(command)
    end

    vim.cmd("augroup end")
end

--
-- Escape a key sequence
--
export.keys = function(sequence)
    return vim.api.nvim_replace_termcodes(sequence, true, true, true)
end

return export
