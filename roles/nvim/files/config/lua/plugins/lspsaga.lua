-- verify the plugin exists
local has = require("utils").has
if not has("lspsaga") then
    return
end

require("lspsaga").setup({
    diagnostic = {
        enable = false,
        show_code_action = false,
        jump_num_shortcut = false,
        keys = {
            quit = { "q", "<ESC>" },
        },
    },
    rename = {
        keys = {
            quit = { "<ESC>" },
        },
    },
    ui = {
        code_action = "",
    },
    symbol_in_winbar = {
        enable = false,
    },
    -- use_saga_diagnostic_sign = false,
    -- diagnostic_header_icon = " ",
    -- code_action_icon = " ",
    -- code_action_prompt = {
    --     enable = false,
    --     sign = true,
    --     sign_priority = 40,
    --     virtual_text = true,
    -- },
    -- code_action_keys = {
    --     quit = "<esc>",
    -- },
    -- rename_prompt_prefix = "",
    -- rename_action_keys = {
    --     quit = "<esc>",
    -- },
})

-- _G.plug_lspsaga_style = function()
--     vim.api.nvim_exec(
--         [[
--             highlight LspSagaCodeActionBorder guifg=#888888 guibg=none
--             highlight LspSagaRenameBorder guifg=#888888 guibg=none
--             highlight LspSagaHoverBorder guifg=#888888 guibg=none
--             highlight LspSagaDiagnosticBorder guifg=#888888 guibg=none
--             highlight LspSagaDiagnosticTruncateLine guifg=#888888 guibg=none
--
--             highlight LspSagaCodeActionTitle guifg=#dfd7a4 guibg=none gui=nocombine
--             highlight LspSagaCodeActionContent guifg=#dfd7a4 guibg=none gui=nocombine
--             highlight LspSagaDiagnosticHeader guifg=#dfd7a4 guibg=none gui=nocombine
--             highlight LspSagaRenamePromptPrefix guifg=#dfd7a4 guibg=none gui=nocombine
--         ]],
--         false
--     )
-- end
--
-- -- custom style for the popups
-- autogroup("lspsaga_style", {
--     { "VimEnter", "*", ":lua plug_lspsaga_style()" },
-- })
