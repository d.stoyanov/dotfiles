local ok, session = pcall(require, "auto-session")
if not ok then
    return
end

session.setup({
    log_level = "error",
})

local env = require("utils").env

-- define the session configuration before loading the module
local root = env.data .. "/sessions/"
vim.g.auto_session_root_dir = root

-- make sure the root directory exists
if vim.fn.isdirectory(vim.fn.expand(root)) == 0 then
    vim.cmd("!mkdir -p " .. root)
end

-- close the tree before saving the session
vim.g.auto_session_pre_save_cmds = { "tabdo NvimTreeClose" }
