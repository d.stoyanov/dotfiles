-- verify the plugin exists
local has = require("utils").has
if not has("kommentary") then
    return
end

require("kommentary.config").configure_language("default", {
    single_line_comment_string = "auto",
    multi_line_comment_strings = "auto",
    prefer_single_line_comments = false,
    hook_function = function()
        require("ts_context_commentstring.internal").update_commentstring()
    end,
})
