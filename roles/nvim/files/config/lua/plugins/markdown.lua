-- languages to highlight
vim.g.vim_markdown_fenced_languages = {
    "json=javascript",
    "js=javascript",
    "lua=lua",
    "bash=sh",
}

vim.g.vim_markdown_math = 1
vim.g.vim_markdown_toc_autofit = 0
vim.g.vim_markdown_strikethrough = 1
vim.g.vim_markdown_auto_insert_bullets = 0
vim.g.vim_markdown_new_list_item_indent = 0

vim.g.vim_markdown_folding_level = 1
vim.g.vim_markdown_override_foldtext = 0
vim.g.vim_markdown_folding_disabled = 1
vim.g.vim_markdown_no_extensions_in_markdown = 1
