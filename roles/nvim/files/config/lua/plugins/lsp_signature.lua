-- verify the plugin exists
local has = require("utils").has
if not has("lsp_signature") then
    return
end

-- configure lsp_signature
-- require("lsp_signature").setup({
--     bind = true,
--     toggle_key = nil,
--     floating_window = true,
--     hint_enable = false,
--     -- floating_window_above_first = false,
--     floating_window_above_cur_line = true,
--     extra_trigger_chars = { "(", "," },
--     handler_opts = {
--         border = "none",
--     },
-- })
