-- verify the plugin exists
local has = require("utils").has
if not has("FTerm") then
    return
end

local api = vim.api
local autogroup = require("utils").autogroup

-- configure the float term
local term = require("FTerm")
term.setup({
    dimensions = {
        height = 0.9,
        width = 0.9,
        x = 0.5,
        y = 0.3,
    },
    border = "single",
})

-- Fired when  a terminal is cxreated or focused. Automatically enter
-- the terminal mode instead of the default normal mode for the terminal
_G.plug_term_on_enter = function()
    local buf = api.nvim_get_current_buf()
    local bt = api.nvim_buf_get_option(buf, "buftype")

    if bt == "terminal" then
        vim.cmd("call feedkeys('a', 'x')")
    end
end

autogroup("terminal", {
    { "BufEnter", "*", "lua plug_term_on_enter()" },
    { "TermOpen", "*", "lua plug_term_on_enter()" },
})
