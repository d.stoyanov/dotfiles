-- verify the plugin exists
local has = require("utils").has
if not has("lspkind") then
    return
end

-- symbols for autocomplete
-- require("lspkind").init({
--     with_text = false,
--     symbol_map = {
--         Text = "  ",
--         Method = " ƒ ",
--         Function = " ƒ ",
--         Constructor = " ƒ ",
--         Variable = "  ",
--         Class = "  ",
--         Interface = " 蘒",
--         Module = "  ",
--         Property = " ﰠ ",
--         Field = " ﰠ ",
--         Unit = " 塞 ",
--         Value = "  ",
--         Enum = " 練",
--         Keyword = "  ",
--         Snippet = "  ",
--         Color = "  ",
--         File = "  ",
--         Folder = " ﱮ ",
--         EnumMember = "  ",
--         Constant = "  ",
--         Struct = " פּ ",
--         Event = "  ",
--         Operator = "  ",
--     },
-- })
