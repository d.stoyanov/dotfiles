local export = {}
local buf_map = vim.api.nvim_buf_set_keymap

--
-- Enable the word wrapping in the current buffer
--
export.wrap = function()
    -- TODO: migrate to lua at some point
    vim.cmd("setlocal concealcursor=nc")
    vim.cmd("setlocal cc=")
    vim.cmd("setlocal wrap linebreak")
    vim.cmd("setlocal formatoptions=tawcqn")

    -- wrap the file
    vim.cmd("normal! gggqG")

    -- Break undo sequences into chunks (after punctuation)
    buf_map(0, "i", "!", "!<c-g>u", { noremap = true, silent = true })
    buf_map(0, "i", ",", ",<c-g>u", { noremap = true, silent = true })
    buf_map(0, "i", ".", ".<c-g>u", { noremap = true, silent = true })
    buf_map(0, "i", ":", ":<c-g>u", { noremap = true, silent = true })
    buf_map(0, "i", ";", ";<c-g>u", { noremap = true, silent = true })
    buf_map(0, "i", "?", "?<c-g>u", { noremap = true, silent = true })
end

--
-- Disable the word wrapping in the current buffer
--
export.nowrap = function()
    local width = vim.o.textwidth

    vim.cmd("setlocal cc=" .. width)
    vim.cmd("setlocal nowrap")

    -- wrap the file
    vim.cmd("setlocal tw=999")
    vim.cmd("normal! gggqG")
    vim.cmd("setlocal tw=" .. width)

    -- restore the format options
    vim.cmd("setlocal formatoptions=jcroql")
end

-- make the functions global
_G.plug_wrap = export.wrap
_G.plug_nowrap = export.nowrap

-- define the wrap command
vim.cmd("command! Wrap :lua plug_wrap()")
vim.cmd("command! NoWrap :lua plug_nowrap()")

return export
