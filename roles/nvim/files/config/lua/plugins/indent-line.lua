local autogroup = require("utils").autogroup
local has = require("utils").has

if not has("ibl") then
    return
end
local indent = require("ibl")

indent.setup()

-- set the indent character color
autogroup("indent", {
    {
        "ColorScheme",
        "*",
        "highlight IndentBlanklineChar guifg=#403e3e gui=nocombine",
    },
})
