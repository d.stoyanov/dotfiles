local wk = require("which-key")

-- wk.setup({
--     win = {
--         border = "single",
--         position = "bottom",
--         margin = { 1, 2, 1, 2 },
--         padding = { 1, 2, 1, 2 },
--     },
--
--     layout = {
--         height = { min = 4, max = 25 }, -- min and max height of the columns
--         width = { min = 20, max = 50 }, -- min and max width of the columns
--         spacing = 3,
--         align = "left",
--     },
--
--     -- configure the triggers
--     triggers = { "<auto>" },
--     -- triggers = {"<leader>"}
-- })
--
-- -- mappings without a preffix
-- wk.register({
--     ["<s-u>"] = "Redo",
--     ["<f1>"] = "Show this help",
--     ["<f5>"] = "Show the undo tree",
--     ["<c-q>"] = "Close the current buffer",
--
--     ["<s-k>"] = "LSP hover",
--     ["<c-n>"] = "Go to the next diagnostics messages",
--     ["<c-p>"] = "Go to the previous diagnostics messages",
--
--     ["<a-k>"] = "Move selected lines up",
--     ["<a-j>"] = "Move selected lines down",
--
--     ["<c-l>"] = "Focus the split on the left",
--     ["<c-h>"] = "Focus the split on the right",
--     ["<c-j>"] = "Focus the split below",
--     ["<c-k>"] = "Focus the split above",
--
--     -- ignore some mappings
--     [";"] = "which_key_ignore",
-- }, {})
--
-- wk.register({
--     ["<leader>"] = {
--         ["/"] = "Toggle a comment",
--         ["<space>"] = "Open previous buffer",
--         ["e"] = "Toggle the file tree",
--
--         f = {
--             name = "find",
--             f = "Find a file",
--             b = "Find a buffer",
--             g = "Grep in the project folder",
--             h = "Search through the help",
--         },
--
--         g = {
--             name = "git",
--             b = "Toggle git blame",
--             f = "3 way diff: pick from the left",
--             j = "3 way diff: pick from the right",
--             s = "Git status",
--             l = "Git log",
--         },
--
--         o = {
--             name = "open",
--             l = "Open location list",
--             q = "Open quick fix",
--         },
--
--         s = {
--             name = "spelling/surround",
--             a = "Add surround text",
--             d = "Remove surround text",
--             c = "Change surround text",
--             ["["] = "Fix the previous misspelled word",
--             ["]"] = "Fix the next misspelled word",
--             ["."] = "Fix the word under the fursor",
--         },
--
--         n = {
--             name = "notes",
--             f = "Find a file in the notes",
--             g = "Grep in the notes folder",
--             i = "Open the notes index file",
--         },
--     },
-- })
