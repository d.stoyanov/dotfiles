-- color
vim.cmd(
    "autocmd FileType NvimTree highlight NvimTreeFolderIcon guifg=lightgrey"
)

-- disable the sign column in the tree view
-- vim.cmd("autocmd FileType NvimTree setlocal signcolumn=")

require("nvim-tree").setup({
    disable_netrw = true,
    hijack_netrw = true,
    hijack_directories = {
        enable = false,
        auto_open = false,
    },
    open_on_tab = false,
    hijack_cursor = true,
    update_cwd = false,
    update_focused_file = {
        enable = true,
        update_cwd = false,
        ignore_list = {},
    },
    system_open = {
        cmd = nil,
        args = {},
    },
    view = {
        width = 40,
        side = "left",
    },
    filters = {
        dotfiles = true,
        custom = { "node_modules", "pnpm-lock.yaml" },
    },
    renderer = {
        highlight_git = true,
        indent_markers = {
            enable = true,
        },
        icons = {
            webdev_colors = true,
            padding = " ",
            show = {
                file = true,
                folder = true,
                folder_arrow = false,
                git = false,
            },
            glyphs = {
                default = "",
                symlink = "",
                folder = {
                    arrow_closed = "",
                    arrow_open = "",
                    default = "",
                    open = "",
                    empty = "",
                    empty_open = "",
                    symlink = "",
                    symlink_open = "",
                },
            },
        },
    },
    actions = {
        open_file = {
            quit_on_open = true,
            resize_window = false,
            window_picker = {
                enable = false,
            },
        },
    },
})
