-- verify the plugin exists
local has = require("utils").has;
if (not has("nvim-ts-autotag")) then return end

require("nvim-ts-autotag").setup()

