-- verify the plugin exists
local has = require("utils").has
if not has("nvim-autopairs") then
    return
end

-- configure the plugin
local pairs = require("nvim-autopairs")
pairs.setup({ disable_filetype = { "TelescopePrompt", "vim" } })

-- insert `(` after select function or method item
-- if has("cmp") then
--     local cmp_autopairs = require("nvim-autopairs.completion.cmp")
--     local cmp = require("cmp")

--     cmp.event:on(
--         "confirm_done",
--         cmp_autopairs.on_confirm_done({
--             map_char = { tex = "" },
--         })
--     )
-- end
