-- verify the plugin exists
local has = require("utils").has
if not has("nvim-treesitter") then
    return
end

require("nvim-treesitter.configs").setup({
    ensure_installed = "all",
    highlight = { enable = true },
    ignore_install = { "phpdoc", "swift" },
    indent = { enable = true },
})
