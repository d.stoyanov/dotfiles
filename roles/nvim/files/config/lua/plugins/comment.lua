-- verify the plugin exists
local has = require("utils").has
if not has("Comment") then
    return
end

require("Comment").setup({
    ---Add a space b/w comment and the line
    padding = true,

    ---Whether the cursor should stay at its position
    sticky = true,

    ---LHS of toggle mappings in NORMAL + VISUAL mode
    toggler = {
        line = "gcc",
        block = "gbc",
    },

    ---LHS of operator-pending mappings in NORMAL + VISUAL mode
    opleader = {
        line = "gc",
        block = "gb",
    },

    mappings = {
        ---Includes `gcc`, `gbc`, `gc[count]{motion}` and `gb[count]{motion}`
        basic = true,

        ---Includes `gco`, `gcO`, `gcA`
        extra = true,
    },

    --[[ pre_hook = function(ctx) ]]
    --[[     local U = require("Comment.utils") ]]
    --[[]]
    --[[     local location = nil ]]
    --[[     if ctx.ctype == U.ctype.block then ]]
    --[[         location = ]]
    --[[             require("ts_context_commentstring.utils").get_cursor_location() ]]
    --[[     elseif ctx.cmotion == U.cmotion.v or ctx.cmotion == U.cmotion.V then ]]
    --[[         location = ]]
    --[[             require("ts_context_commentstring.utils").get_visual_start_location() ]]
    --[[     end ]]
    --[[]]
    --[[     return require("ts_context_commentstring.internal").calculate_commentstring({ ]]
    --[[         key = ctx.ctype == U.ctype.line and "__default" or "__multiline", ]]
    --[[         location = location, ]]
    --[[     }) ]]
    --[[ end, ]]
})

-- local ft = require("Comment.ft")
-- ft.set("svelte", { "<!-- %s -->", "<!-- %s -->" })
