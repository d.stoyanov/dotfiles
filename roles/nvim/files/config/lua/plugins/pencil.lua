local export = {}

vim.g["pencil#wrapModeDefault"] = "soft"
vim.g["pencil#conceallevel"] = 0

export.init = function()
    vim.api.nvim_call_function("pencil#init", {})
end

return export
