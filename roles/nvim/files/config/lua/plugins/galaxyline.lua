-- verify the plugin exists
local has = require("utils").has
if not has("galaxyline") then
    return
end

local gl = require("galaxyline")
local gls = gl.section
local condition = require("galaxyline.condition")
local fileinfo = require("galaxyline.provider_fileinfo")
local cmd = vim.api.nvim_command

-- supported mode styles
local Mode = { NORMAL = "n", INSERT = "i", VISUAL = "v", REPLACE = "r" }

-- theme of the line
local colors = {
    bg = "#504945",
    fg = "#e5d7b3",
    separator = "#3e3e3e",
    inactive = "#303030",
    cyan = "#008080",
    green = "#98be65",
    orange = "#fc802d",
    violet = "#a9a1e1",
    magenta = "#c678dd",
    blue = "#51afef",
    red = "#ec5f67",
    black = "#38332a",
    white = "#a89985",
}

-- show the short line for these file types
gl.short_line_list = {
    "NvimTree",
    "LuaTree",
    "vista",
    "dbui",
    "startify",
    "term",
    "nerdtree",
    "fugitive",
    "fugitiveblame",
    "plug",
}

-- return the vim mode information
local get_mode = function()
    -- map the modes to the recognized values
    local mode_map = {
        n = Mode.NORMAL,
        i = Mode.INSERT,
        V = Mode.VISUAL,
        [""] = Mode.VISUAL,
        v = Mode.VISUAL,
        R = Mode.REPLACE,
        r = Mode.REPLACE,
        Rv = Mode.REPLACE,
    }

    -- define mode name alias
    local mode_label = {
        [Mode.NORMAL] = "NORMAL",
        [Mode.INSERT] = "INSERT",
        [Mode.VISUAL] = "VISUAL",
        [Mode.REPLACE] = "REPLACE",
    }

    --  mode background colors
    local background = {
        [Mode.NORMAL] = colors.white,
        [Mode.INSERT] = colors.green,
        [Mode.VISUAL] = colors.orange,
        [Mode.REPLACE] = colors.violet,
    }

    local mode = mode_map[vim.fn.mode()] or Mode.NORMAL
    return {
        mode = mode,
        label = mode_label[mode],
        bg = background[mode],
        fg = colors.black,
    }
end

-- add a new segment to the line
local add_segment = function(position, args)
    local group = gls[position]

    -- set defaults
    for _, config in pairs(args) do
        config.highlight = config.highlight or { colors.fb, colors.bg }
        config.separator = config.separator or " "
        config.separator_highlight = config.separator_highlight
            or { colors.separator, colors.bg }
    end

    -- add it to the group
    table.insert(group, args)
end

local segment = {
    left = function(args)
        add_segment("left", args)
    end,
    right = function(args)
        add_segment("right", args)
    end,
}

-- show the vim mode
segment.left({
    ViMode = {
        provider = function()
            local mode = get_mode()
            local segments = { "ViMode" }

            for _, name in ipairs(segments) do
                cmd("hi Galaxy" .. name .. " guifg=" .. mode.fg)
                cmd("hi Galaxy" .. name .. " guibg=" .. mode.bg)
            end

            return string.format("  %s ", mode.label)
        end,
        highlight = { colors.black, colors.white, "bold" },
    },
})

local function file_readonly()
    if vim.bo.filetype == "help" then
        return ""
    end

    if vim.bo.readonly == true then
        return "  "
    end

    return ""
end

-- get current file name
local function file_name()
    local file = vim.fn.expand("%:f")

    if vim.fn.empty(file) == 1 then
        return ""
    end

    if string.len(file_readonly()) ~= 0 then
        return file .. file_readonly()
    end

    if vim.bo.modifiable then
        if vim.bo.modified then
            return file .. "  "
        end
    end

    return file .. " "
end

-- show the file name
segment.left({
    FileName = {
        provider = file_name,
        condition = condition.buffer_not_empty,
    },
})

-- errors
segment.right({
    Errors = {
        provider = "DiagnosticError",
        icon = "  ",
        highlight = { colors.red, colors.bg },
        separator = "",
    },
})

-- warnings
segment.right({
    Warnings = {
        provider = "DiagnosticWarn",
        icon = "  ",
        highlight = { colors.orange, colors.bg },
        separator = "",
    },
})

-- info
segment.right({
    Info = {
        provider = "DiagnosticInfo",
        icon = "  ",
        highlight = { colors.blue, colors.bg },
        separator = "",
    },
})

-- hints
segment.right({
    Hints = {
        provider = "DiagnosticHint",
        icon = "  ",
        highlight = { colors.blue, colors.bg },
        separator = "",
    },
})

-- whitespace
segment.right({
    WhiteSpace = {
        provider = "WhiteSpace",
        highlight = { colors.orange, colors.bg },
        separator = "",
    },
})

-- line/column information
segment.right({
    Line = {
        provider = function()
            local info = require("galaxyline.provider_fileinfo")
            local line = info.line_column()
            local percent = info.current_line_percent()

            return line .. " " .. vim.fn.trim(percent)
        end,
        separator = " │ ",
    },
})

-- show git branch
segment.right({
    Git = {
        condition = condition.check_git_workspace,
        provider = function()
            local branch = require("galaxyline.provider_vcs").get_git_branch()
            return " " .. vim.fn.trim(branch)
        end,
        separator = " │ ",
    },
})

-- show file encodding / format
segment.right({
    FileFormat = {
        condition = condition.hide_in_width,
        provider = function()
            local encoding = vim.bo.fileencoding
            local format = vim.bo.fileformat
            return string.format("%s [%s]", encoding, format)
        end,
        separator = " │ ",
    },
})

-- show the buffer type
-- segment.right({
--     BufferType = {
--         condition = condition.hide_in_width,
--         provider = "FileTypeName",
--         separator = " │ ",
--     },
-- })

--  space at the end of the line
segment.right({
    End = {
        provider = function()
            return " "
        end,
    },
})

-- spacer at the start of the line
table.insert(gls.short_line_left, {
    Start = {
        provider = function()
            return " "
        end,
        highlight = { colors.fg, colors.inactive },
    },
})

-- file name
table.insert(gls.short_line_left, {
    SFileName = {
        provider = "SFileName",
        condition = condition.buffer_not_empty,
        highlight = { colors.fg, colors.inactive },
    },
})
