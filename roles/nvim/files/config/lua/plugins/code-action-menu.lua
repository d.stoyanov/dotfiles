-- verify the plugin exists
local has = require("utils").has
local autogroup = require("utils").autogroup
if not has("code_action_menu") then
    return
end

vim.g.code_action_menu_show_details = false
vim.g.code_action_menu_show_diff = false

_G.plug_code_actions_style = function()
    vim.api.nvim_exec(
        [[
            highlight CodeActionMenuMenuKind  guifg=#dfd7a4 guibg=none gui=nocombine
            highlight CodeActionMenuMenuTitle guifg=#dfd7a4 guibg=none gui=nocombine
        ]],
        false
    )
end

-- custom style for the popups
autogroup("code_actions_style", {
    { "VimEnter", "*", ":lua plug_code_actions_style()" },
})
