-- verify the plugin exists
local has = require("utils").has
if not has("telescope") then
    return
end

-- update some defaults
require("telescope").setup({})

local builtin = require("telescope.builtin")
local export = {}
local themes = {}

function themes.dropdown(opts)
    opts = opts or {}
    local defaults = {
        winblend = 15,
        prompt = " ",
        previewer = false,
        sorting_strategy = "ascending",
        layout_strategy = "center",
        layout_config = { width = 0.8, height = 0.8 },
        results_title = false,
        borderchars = {
            { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
            prompt = { "─", "│", " ", "│", "╭", "╮", "│", "│" },
            results = {
                "─",
                "│",
                "─",
                "│",
                "├",
                "┤",
                "╯",
                "╰",
            },
            preview = {
                "─",
                "│",
                "─",
                "│",
                "╭",
                "╮",
                "╯",
                "╰",
            },
        },
    }

    return vim.tbl_deep_extend("force", defaults, opts)
end

function themes.preview(opts)
    opts = opts or {}
    local defaults = {
        sorting_strategy = "ascending",
        prompt_title = "Search",
        results_title = "",
        preview_title = "",
        winblend = 15,
        layout_strategy = "horizontal",
        layout_config = { prompt_position = "top", width = 0.8, height = 0.8 },
    }

    return vim.tbl_deep_extend("force", defaults, opts)
end

export.files = function()
    local options = themes.dropdown()
    builtin.find_files(options)
end

export.grep = function()
    local options = themes.preview()
    builtin.live_grep(options)
end

export.help = function()
    local options = themes.preview()
    builtin.help_tags(options)
end

export.buffers = function()
    local options = themes.dropdown()
    builtin.buffers(options)
end

export.notes_files = function()
    local options = themes.dropdown({ search_dirs = { "~/Notes/" } })
    builtin.find_files(options)
end

export.notes_grep = function()
    local options = themes.preview({ search_dirs = { "~/Notes/" } })
    builtin.live_grep(options)
end

export.references = function()
    local options = themes.preview()
    builtin.lsp_references(options)
end

export.definition = function()
    local options = themes.preview()
    builtin.lsp_definitions(options)
end

export.implementation = function()
    local options = themes.preview()
    builtin.lsp_implementations(options)
end

export.git_branches = function()
    local options = themes.preview()
    builtin.git_branches(options)
end

export.git_commits = function()
    local options = themes.preview()
    builtin.git_commits(options)
end

export.commands = function()
    local options = themes.preview()
    builtin.commands(options)
end

return export
