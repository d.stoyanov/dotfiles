-- verify the plugin exists
local has = require("utils").has
if not has("cmp") then
    return
end

local cmp = require("cmp")

local has_words_before = function()
    unpack = unpack or table.unpack
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0
        and vim.api
                .nvim_buf_get_lines(0, line - 1, line, true)[1]
                :sub(col, col)
                :match("%s")
            == nil
end

local feedkey = function(key, mode)
    vim.api.nvim_feedkeys(
        vim.api.nvim_replace_termcodes(key, true, true, true),
        mode,
        true
    )
end

-- Set up nvim-cmp.
cmp.setup({
    completion = {
        keyword_length = 2,
        completeopt = "menu,menuone,noinsert",
    },
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        end,
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        ["<C-b>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif vim.fn["vsnip#available"](1) == 1 then
                feedkey("<Plug>(vsnip-expand-or-jump)", "")
            elseif has_words_before() then
                cmp.complete()
            else
                fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
            end
        end, { "i", "s" }),

        ["<S-Tab>"] = cmp.mapping(function()
            if cmp.visible() then
                cmp.select_prev_item()
            elseif vim.fn["vsnip#jumpable"](-1) == 1 then
                feedkey("<Plug>(vsnip-jump-prev)", "")
            end
        end, { "i", "s" }),

        ["<CR>"] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        }),
    }),
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "vsnip" },
        { name = "path" },
    }, {
        { name = "buffer" },
    }),
})

-- Set configuration for specific filetype.
cmp.setup.filetype("gitcommit", {
    sources = cmp.config.sources({
        { name = "cmp_git" }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
        { name = "buffer" },
    }),
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline({ "/", "?" }, {
--     mapping = cmp.mapping.preset.cmdline(),
--     sources = {
--         { name = "buffer" },
--     },
-- })

-- -- Set up lspconfig.
-- local capabilities = require("cmp_nvim_lsp").default_capabilities()
-- -- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
-- require("lspconfig")["<YOUR_LSP_SERVER>"].setup({
--     capabilities = capabilities,
-- })

-- cmp.setup({
--     completion = {
--         completeopt = "menu,menuone,noselect",
--         -- autocomplete = {}, -- disable autocomplete
--         autocomplete = { cmp.TriggerEvent.TextChanged },
--     },
--
--     mapping = {
--         ["<C-n>"] = cmp.mapping.select_next_item({
--             behavior = cmp.SelectBehavior.Insert,
--         }),
--         ["<C-p>"] = cmp.mapping.select_prev_item({
--             behavior = cmp.SelectBehavior.Insert,
--         }),
--         ["<Down>"] = cmp.mapping.select_next_item({
--             behavior = cmp.SelectBehavior.Select,
--         }),
--         ["<Up>"] = cmp.mapping.select_prev_item({
--             behavior = cmp.SelectBehavior.Select,
--         }),
--         ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
--         ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
--         ["<C-space>"] = cmp.mapping.complete(),
--         ["<C-e>"] = cmp.mapping.close(),
--         -- ["<ESC>"] = cmp.mapping.close(),
--         ["<CR>"] = cmp.mapping.confirm({
--             behavior = cmp.ConfirmBehavior.Replace,
--             select = true,
--         }),
--         ["<Tab>"] = function(fallback)
--             if cmp.visible() then
--                 cmp.select_next_item()
--             elseif vim.fn["vsnip#available"]() == 1 then
--                 vim.fn.feedkeys(keys("<Plug>(vsnip-expand-or-jump)"), "")
--                 -- elseif not check_backspace() then
--                 --     cmp.mapping.complete()(fallback)
--             else
--                 fallback()
--             end
--         end,
--         ["<S-Tab>"] = function(fallback)
--             if cmp.visible() then
--                 cmp.select_prev_item()
--             elseif vim.fn["vsnip#jumpable"](-1) == 1 then
--                 vim.fn.feedkeys(keys("<Plug>(vsnip-jump-prev)"), "")
--             else
--                 fallback()
--             end
--         end,
--     },
--
--     sources = cmp.config.sources({
--         { name = "nvim_lsp" },
--         { name = "path" },
--         { name = "vsnip" },
--         { name = "buffer", keyword_length = 3 },
--     }),
--
--     snippet = {
--         expand = function(args)
--             vim.fn["vsnip#anonymous"](args.body)
--         end,
--     },
--
--     -- experimental = {
--     --     native_menu = false,
--     --     ghost_text = true,
--     -- },
--
--     formatting = {
--         -- Youtube: How to set up nice formatting for your sources.
--         format = lspkind.cmp_format({
--             -- with_text = true,
--             menu = {
--                 buffer = "[buf]",
--                 nvim_lsp = "[lsp]",
--                 nvim_lua = "[api]",
--                 path = "[path]",
--                 luasnip = "[snip]",
--                 vsnip = "[snip]",
--                 gh_issues = "[issues]",
--             },
--         }),
--     },
-- })

-- -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline("/", {
--     sources = {
--         { name = "buffer" },
--     },
-- })

-- -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
-- cmp.setup.cmdline(":", {
--     sources = cmp.config.sources({
--         { name = "path" },
--     }, {
--         { name = "cmdline" },
--     }),
-- })
