local g = vim.g

local function map(mode, lhs, rhs, opts)
    opts = opts or { silent = true }
    local options =
        setmetatable(opts, { __index = { noremap = true, silent = true } })
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- change the leader
map("n", "<space>", "<nop>")
g.mapleader = " "

-- use ; instead of :
-- map("n", ";", ":")
-- map("v", ";", ":")

-- switch to the previous buffer
map("n", "<leader><leader>", "<c-^>")

-- fix the wildmenu key maps
map("c", "<up>", 'pumvisible() ? "<c-p>" : "\\<up>"', { expr = true })
map("c", "<down>", 'pumvisible() ? "<c-n>" : "\\<down>"', { expr = true })
map("c", "<cr>", 'pumvisible() ? "<c-Y>" : "\\<cr>"', { expr = true })
-- map("c", "<left>", 'pumvisible() ? "<up>" : "\\<left>"', { expr = true })
-- map("c", "<right>", 'pumvisible() ? "<down>" : "\\<right>"', { expr = true })

-- shift U does redo
map("n", "<s-u>", "<c-r>")

-- use jj instead of escape
map("i", "jj", "<escape>")

-- avoid unintentional switches to Ex mode.
map("n", "Q", "<nop>")

-- paste in insert mode
map("i", "<c-p>", "<esc>pa")

-- easier split navigation
map("n", "<c-j>", "<c-w>j")
map("n", "<c-k>", "<c-w>k")
map("n", "<c-h>", "<c-w>h")
map("n", "<c-l>", "<c-w>l")

-- better navigation in insert mode
map("i", "<c-h>", "<left>")
map("i", "<c-j>", "<down>")
map("i", "<c-k>", "<up>")
map("i", "<c-l>", "<right>")

-- move lines around
map("n", "<a-j>", ":m .+1<cr>==")
map("n", "<a-k>", ":m .-2<cr>==")
map("i", "<a-j>", "<esc>:m .+1<cr>==gi")
map("i", "<a-k>", "<esc>:m .-2<cr>==gi")
map("v", "<a-j>", ":m '>+1<cr>gv=gv")
map("v", "<a-k>", ":m '<-2<cr>gv=gv")

-- sell checker convenience
map("n", "<leader>s[", "[sz=1<cr><cr>")
map("n", "<leader>s]", "]sz=1<cr><cr>")
map("n", "<leader>s.", "z=1<cr><cr>")

-- quick fix navigation
map("n", "<leader>oq", ":copen<CR>")
map("n", "<leader>ol", ":lopen<CR>")

-- explorer
map("n", "<leader>e", ":NvimTreeToggle<cr>")

-- telescope
map("n", "<leader>ff", "<cmd>lua require('plugins.telescope').files()<cr>")
map("n", "<leader>fg", "<cmd>lua require('plugins.telescope').grep()<cr>")
map("n", "<leader>fh", "<cmd>lua require('plugins.telescope').help()<cr>")
map("n", "<leader>fb", "<cmd>lua require('plugins.telescope').buffers()<cr>")
map("n", "<leader>c", "<cmd>lua require('plugins.telescope').commands()<cr>")

-- close the current buffer
map("n", "<c-q>", ":Bdelete<cr>")

-- commenting
map("n", "<leader>/", "<plug>kommentary_line_default")
map("v", "<leader>/", "<plug>kommentary_visual_default")

-- lsp
-- map("n", "gd", "<cmd>lua vim.lsp.buf.definition()<cr>")
map("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<cr>")
-- map("n", "gR", "<cmd>lua vim.lsp.buf.references()<cr>")
-- map("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<cr>")

-- telescope lsp navigation
map("n", "gR", "<cmd>lua require('plugins.telescope').references()<cr>")
map("n", "gd", "<cmd>lua require('plugins.telescope').definition()<cr>")
map("n", "gi", "<cmd>lua require('plugins.telescope').implementation()<cr>")

map("n", "ca", ":CodeActionMenu<cr>")
map("n", "K", ":Lspsaga hover_doc<cr>")
map("n", "[d", ":Lspsaga diagnostic_jump_prev<cr>")
map("n", "]d", ":Lspsaga diagnostic_jump_next<cr>")
map("n", "gr", ":Lspsaga rename<cr>")
map("n", "gs", ":Lspsaga signature_help<cr>")

-- git
map("n", "<leader>gs", ":G<cr>")
map("n", "<leader>gl", ":G log<cr>")
map(
    "n",
    "<leader>gc",
    "<cmd>lua require('plugins.telescope').git_commits()<cr>"
)
map(
    "n",
    "<leader>gb",
    "<cmd>lua require('plugins.telescope').git_branches()<cr>"
)
map("n", "<leader>g?", ":GitBlameToggle<cr>")

-- pick left/right while resolving git conflicts
map("n", "<leader>gj", ":diffget //3<cr>")
map("n", "<leader>gf", ":diffget //2<cr>")

-- undotree
map("n", "<f5>", ":UndotreeToggle<cr>")

-- which key
map("n", "<f1>", ":WhichKey<cr>")

-- surround
map("n", "<leader>sa", "ys", { noremap = false })
map("n", "<leader>sd", "ds", { noremap = false })
map("n", "<leader>sc", "cs", { noremap = false })

-- notes
map("n", "<leader>ni", ":e ~/Notes/index.md<cr>")
map(
    "n",
    "<leader>nf",
    "<cmd>lua require('plugins.telescope').notes_files()<cr>"
)
map("n", "<leader>ng", "<cmd>lua require('plugins.telescope').notes_grep()<cr>")

-- term
map("n", "<c-t>", "<cmd>lua require('FTerm').toggle()<cr>")
map("t", "<c-t>", "<c-\\><c-n><cmd>lua require('FTerm').toggle()<cr>")

map("t", "<esc>", "<c-\\><c-n>")

map("t", "<c-j>", "<c-\\><c-n><c-w>j")
map("t", "<c-k>", "<c-\\><c-n><c-w>k")
map("t", "<c-h>", "<c-\\><c-n><c-w>h")
map("t", "<c-l>", "<c-\\><c-n><c-w>l")

-- vimux
map("n", "<leader>tc", ":VimuxPromptCommand<cr>")
map("n", "<leader>tt", ":VimuxRunLastCommand<cr>")
map("n", "<leader>ti", ":VimuxInspectRunner<cr>")
