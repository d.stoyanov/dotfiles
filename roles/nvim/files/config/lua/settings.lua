local g = vim.g
local o = vim.o
local w = vim.wo
local cmd = vim.cmd

-- global options
o.mouse = "a" -- enable mouse support
o.showmatch = true -- show the matching brackets
o.ignorecase = true -- case insensitive search
o.smartcase = true -- make the search smarter
o.hlsearch = true -- highlight all matches
o.showcmd = true -- show the current command
o.hidden = true -- navigate away from buffers without saving
o.splitright = true -- open the new splits to the right
o.splitbelow = true -- open the vertical splits below
o.showbreak = " ↳ " -- show a prefix for the wrapped lines
o.scrolloff = 8 -- keep the cursor away from the edge
o.cmdheight = 0 -- more space for displaying messages.
o.showmode = false -- do not show the mode

o.fileformats = "unix,dos,mac"
o.listchars = "tab:▸ ,trail:~,extends:#,nbsp:·"
o.list = true

-- indentation
o.tabstop = 4 -- set the tab with to 4 spaces
o.softtabstop = 4
o.expandtab = true -- replace tab with spaces
o.autoindent = true -- enable auto indent
o.shiftwidth = 4 -- auto indent spaces
o.shiftround = true -- smart indentation to the nearest indent level

-- make <C-a> and <C-x> play well with zero-padded numbers
-- (i.e. don't consider them octal or hex)
o.nrformats = ""

-- Having longer update time (default is 4000ms = 4s) leads to noticeable
-- delays and poor user experience.
o.updatetime = 300

o.clipboard = "unnamedplus" -- normal OS clipboard interaction
o.autoread = true -- automatically reload files changed outside of Vim
o.encoding = "utf-8"

-- undo history and backups
o.history = 1000 -- bigger search history
o.undolevels = 1000 -- use many levels of undo
o.undofile = true -- keep a persistent backup file
o.backup = false -- do not keep backup files
o.swapfile = false -- do not write swap files
o.undodir = vim.fn.stdpath("data") .. "/undo/"
o.directory = "~/.vim/.tmp,~/tmp,/tmp"

-- spell checker
w.spell = true -- enable spell checker
o.spelllang = "en_us" -- set the default spell language

-- window options
w.number = true -- show the line numbers
w.relativenumber = true -- enable relative numbers
o.textwidth = 80 -- the width of the text
w.cc = "+1" -- show a line at textwidth
w.cursorline = true -- highlight the current line
w.wrap = false -- disable line wrapping
w.signcolumn = "yes" -- always show the sign column
w.linebreak = true -- word wrap
w.foldmethod = "indent" -- set default fold method
w.foldenable = false -- do not fold blocks on open

-- theme
o.termguicolors = true
o.background = "dark"
o.syntax = "on"

g.gruvbox_material_background = "medium"
g.gruvbox_material_enable_italic = 1
g.gruvbox_material_palette = "original" -- one of material, mix or original

cmd("colorscheme gruvbox-material")
vim.api.nvim_set_hl(0, "@tag.attribute", { link = "White" })

-- cursor
-- cmd("highlight Cursor guifg=white guibg=black")
cmd("highlight iCursor guifg=white guibg=black")
-- o.guicursor = "n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50,"
--     .. "a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,"
--     .. "sm:block-blinkwait175-blinkoff150-blinkon175"

-- configure the command line completion menu
o.wildignore = "*.a,*.o,*.so,"
    .. ".git,*.swp,*.tmp,"
    .. "*/build/*,*/deps/*,"
    .. "*/node_modules/*,"
    .. "*.zip,*.tar.*"
o.wildmenu = true
o.wildignorecase = true
o.wildmode = "longest:full,list:full"

-- enable local configuration files
-- o.exrc = true

cmd("filetype plugin on")
cmd("filetype plugin indent on")
