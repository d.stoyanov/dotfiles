-- enable the text wrapping
-- local wrap = require("plugins.wrap")
-- wrap.wrap()

-- enable pencil
require("plugins.pencil").init()

-- enable the spell check
vim.cmd("setlocal spell")
