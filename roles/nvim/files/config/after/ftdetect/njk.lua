-- set the file type for njk files
vim.cmd("au! BufRead,BufNewFile *.njk")
vim.cmd("au BufRead,BufNewFile *.njk set ft=jinja")
