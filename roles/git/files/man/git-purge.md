% git-purge(1) | Git Manual

# NAME

**git-purge** -- purge all merged local branches and orphaned remote ones.

# SYNOPSIS

| **git purge**
| **git purge** [**--keep** &lt;branch1&gt;,&lt;branch2&gt;]
| **git purge** [**--base** &lt;branch&gt;]

# DESCRIPTION

**git-purge** is a shortcut for for cleaning up the repository from all 
branches which have been merged and all remote branches which have been deleted 
from the remote repository.

# OPTIONS

**--keep** &lt;branch&gt;,&lt;branch&gt; ...
: A list of branches separated by space which will be kept from removing while 
doing the clean up. It is also possible to define a list of branches to be 
skipped from removing using the environment variable **GIT_PURGE_KEEP**.

    | export **GIT_PURGE_KEEP**=branch1,branch2,branch3

**--base** &lt;branch&gt;
: The name of the base branch of the repository, which will be used to check 
for merged local branches. There is an alternative method to set this value by 
using an environment variable. If the environment variable **GIT_PURGE_BASE** 
is defined when running the script it's value will be used as a base git 
branch.

**-h, --help**
: Prints brief usage information.

