#!/usr/bin/python

from ansible.module_utils.basic import *
from datetime import datetime
import os
import os.path
import pwd
import stat
import pathlib


def main():
    module = AnsibleModule(argument_spec={
        "src": { "type": 'path', "required": True },
        "dest": { "type": 'path', "required": True },
    })

    src_path = module.params["src"]
    dest_path = module.params["dest"]

    module.log("Link: %s => %s\n" % (src_path, dest_path))

    # resolve the source file
    src = pathlib.Path(src_path).resolve()
    dest = pathlib.Path(dest_path)

    # validate the the source exist and is readable
    if not src.exists():
        module.fail_json(msg="Source %s not found" % (src_path))

    if not os.access(src_path, os.R_OK):
        module.fail_json(msg="Source %s not readable" % (src_path))


    # check if the destination ends with /
    if dest_path.endswith(os.sep):
        dest = dest.joinpath(src.name)

    # create the destination folder structure
    dest.parent.mkdir(parents=True, exist_ok=True)

    # check if the destination exists and is not a link to the same file
    doBackup = False
    if dest.is_symlink():
        target = dest.readlink()

        if str(src) == str(target):
            module.log("symlink %s already exists" % dest_path)
            module.exit_json( changed = False, msg = "link %s exists" % dest_path )
        else:
            doBackup = True

    # check if the target exists and should be backed up
    if dest.exists():
        doBackup = True

    # do the backup
    if doBackup:
        try:
            backup(dest, module)
        except Exception as ex:
            return module.fail_json(msg="Error backing up target file: %s" % (ex))

    # create the symlink
    os.symlink(str(src), str(dest))
    module.log("symlink %s created" % dest_path)

    module.exit_json(
        changed = True,
        msg = "link %s created" % dest_path,
        link = dict( src=str(src_path), dest=str(dest_path) )
    )


def backup(uri, module):
    path = pathlib.Path(uri)
    module.log("%s => %s" % (uri, path))

    # check if the file exists
    if not path.is_symlink():
        if not path.exists():
            raise Exception("Path %s does not exists" % (uri))

        if not os.access(path, os.W_OK):
            raise Exception("Insufficient privileges to backup path %s" % (uri))


    time = datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
    attempt = 0

    while True:
        new_name = "%s.%s" % (path, time)
        if attempt > 0:
            new_name = "%s.%s" % (new_name, attempt)

        if not os.path.exists(new_name):
            path.rename(new_name)

        return new_name



if __name__ == '__main__':
    main()
